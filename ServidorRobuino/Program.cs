﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using SecretLabs.NETMF.Hardware;
using SecretLabs.NETMF.Hardware.NetduinoPlus;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace ServidorRobuino
{
    class Program
    {
        static void Main(string[] args)
        {
            OutputPort Acelerar = new OutputPort(Pins.GPIO_PIN_D0, false);
            OutputPort Re = new OutputPort(Pins.GPIO_PIN_D1, false);
            OutputPort Direita = new OutputPort(Pins.GPIO_PIN_D2, false);
            OutputPort Esquerda = new OutputPort(Pins.GPIO_PIN_D3, false);
            
            //Add porta e ip do servidor
            int i;
            bool Conectado;
            using (System.Net.Sockets.Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
            {
                socket.Bind(new IPEndPoint(IPAddress.Any, 10100));
                socket.Listen(1);
                {
                    while (true)


                        using (Socket NetduinoSocket = socket.Accept())
                        {
                            Conectado = true;
                            byte[] buffer = Encoding.UTF8.GetBytes("Aguardando comandos do Controle...\n");
                            NetduinoSocket.Send(buffer, 0, buffer.Length, 0);


                            while (Conectado)
                            {
                                if (NetduinoSocket.Poll(1, SelectMode.SelectRead))
                                {
                                    byte[] bytes = new byte[10];
                                    NetduinoSocket.Receive(bytes);
                                    char[] chars = Encoding.UTF8.GetChars(bytes);

                                    if (chars.Length > 0)
                                    {
                                        for (i = 0; i <= chars.Length - 1; i++)
                                        {
                                            if (chars[i] == 87)
                                            {
                                                Acelerar.Write(true);
                                                Re.Write(false);                                                
                                                Thread.Sleep(50);                                       
                                            }
                                            if (chars[i] == 65)
                                            {
                                                Esquerda.Write(true);
                                                Direita.Write(false);
                                                Thread.Sleep(50);
                                            }
                                            if (chars[i] == 83)
                                            {
                                                Acelerar.Write(false);
                                                Re.Write(true);                                               
                                                Thread.Sleep(50);                                               
                                            }
                                            if (chars[i] == 68)
                                            {
                                                Direita.Write(true);
                                                Esquerda.Write(false);
                                                Thread.Sleep(50);
                                            }
                                            if (chars[i] == 76)
                                                Conectado = false;
                                        }
                                        Direita.Write(false);
                                        Esquerda.Write(false);
                                        Acelerar.Write(false);
                                        Re.Write(false);
                                    }                                    
                                }
                            }
                            if (Conectado == false)
                                NetduinoSocket.Close();
                        }                    
                }
            }
        }
    }
}